![Build Status](https://gitlab.com/pages/hugo/badges/master/build.svg)

---

Author: Alessandro Guida

README
================================================================================

This is the main website for the ACC-bioinformatic project. It is accessible at the following public link

* https://acc-bioinfo.gitlab.io


Set up
--------------------------------------------------------------------------------

This website was created using the HUGO platform, and GitLab Pages.

To learn more about GitLab Pages: https://pages.gitlab.io , the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

The website was created following this guide:

* https://about.gitlab.com/features/pages/

### Theme info

The theme currently used is "docdock". The documentation is available at the following link:

* http://docdock.netlify.com/content-organisation/

Il tema è stato clonato dal repository dello sviluppatore. E' stato quindi necessario aggiungere un ```git pull submodule``` nel gitlab-ci, per assicurarsi che non ci fossero problemi di versione.  

### Use with docker

The project runs with an older version of hugo. To test it:

1. Build the image
`docker build -t hugo:040 .`

2. Run HUGO:

`docker run --rm -p 1313:1313 -v $(pwd):/usr/src/app hugo:040 hugo serve --bind 0.0.0.0`

And open the url http://localhost:1313/ in your favorite browser.
