+++
title = "home"
+++

# ACC Bioinformatics WorkGroup: public workflow and protocols

This website is maintained by the ACC-bioinformatics workgroup of the [Alleanza Contro il Cancro](http://www.alleanzacontroilcancro.it/en/) network.

Alleanza Contro il Cancro (ACC) is the largest Italian oncology network. 19 Research-driven hospitals (Istituti di Ricovero e Cura a Carattere Scientifico, IRCCS) adhere to ACC.
In 2014-2017 ACC was given a flexible organisation in order to participate in national and international research consortia, and it started creating a working group on genomics, called ACC-Genomics, in order to quickly create several personalized medicine plans in Italy. 

This working group recently developed new panels for genomic analysis of lung and breast cancer, with the final aim to implement them in several ACC-hospitals.

The proposed procedure is already largely used in research but not extensively in clinical routine diagnostics. It consist in the sampling of two
biological samples for each patient, one of a tumor and one germline (blood or other non-tumoral tissue). 
DNA is then extracted from both samples and sequenced via NGS. The sequencers generate data that undergoes automated bioinformatic analyses, for instance matched tumor-normal variant calling.

Since there is no scientific consensus on the analysis process, one of the efforts of the ACC genomics community is to validate a shared bioinformatic procedure among all the participating IRCSS.
To this end, all the raw sequencing data and the analysis pipelines will be shared in a single computational structure. 

The testing and development of the software will be carried out by ACC-Bioinformatics group.