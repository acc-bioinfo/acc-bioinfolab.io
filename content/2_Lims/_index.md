+++
title = "Patient registration (LIMS)"
pre ="<i class='fab fa-wpforms'></i>&nbsp"
weight = 2
+++

Patients and samples should be registered in the ACC LIMS available at https://acc-web.cnaf.infn.it/.
