+++
title = "Web registration"
pre ="<i class='fab fa-wpforms'></i>&nbsp"
weight = 1
+++

Patients and samples should be registered in the ACC LIMS available at https://acc-web.cnaf.infn.it/


## Patient registration

To add a new patient, go to NGS-specialist in the left menu, select patient, and press the button "+ add patient"

You should add the following information:

* **internal id (optional)**: Patient identifier used internally at the hospital or At Ieo, for example the internal patient identifier could be something like: "IEO1234". This field is only useful for the hospital where the patient is coming from allowing a fast ID conversion. Important: this ID needs tobe anonimized.
* **CRO id**: Patient identifier provided by the CRO. This is the unique information that can help associate a patient inserted in the list to teh information stored by the CRO.
* **Project**: one of the ACC projects from the drop down list. If your project is not available, please contact the administrators.
* **Tss** (Tissue source site): IRCCS from where the sample was collected

Once the patient is saved, a unique barcode will be generated. Only the internal and CRO id can be modified after the barcode generation. Only the curator of the patient and the administrators can update a patient. If you need to remove a patient, please contact the administrators.


## Sample registration

To add a new sample, either press the "+ new sample" button in the patient table, or go to NGS-specialist in the left menu, select sample, and press the button "+ add sample"

You should add the following information:

* **Sample label**: Temporary sample label used only for temporary internal identification during laboratory procedures. The idea of this label is to be short enough to use it as a label (max 20 characters allowed).
* **Collection date**: Date on which the sample was received.
* **Patient**: patient identifier. It will be automatically filled if you come from the patient page.
* **Sample type**: Specify if the sample is coming from a tumor tissue or a normal tissue.
* **Tissue** from which the sample was collected (e.g. blood, saliva, biopsy, etc..).
* **Sample preservation**: Type of sample conservation (e.g, paraffin, frozen).
* **Sample cellularity**: Cellularity of the sample (percentage), it should be an integer between 0 and 100.

If the type of tissue, sample preservation of type of sample is missing, please contact the administrators.

Once the sample is saved, a unique barcode will be generated. Only a few fields can be modified after the barcode generation. Only the curator of the sample and the administrators can update a sample. If you need to remove a sample, please contact the administrators.



## Sequencing Batch (run) Registration

To add a new sequencing batch (sequencing run), go to NGS-specialist in the left menu, select sequencing batch, and press the button "+ add sequencing batch"

You should add the following information:

* **Sc id**: Sequencing Center, Irccs where the sample is sequenced.
* **Sequencer**: Sequencer model for sequencing the sample.
* **Sequencing Date**: Date when the sequencing was performed.
* **Description**: (Optional), test with a description of the samplein the sequencing batch.

If the type of Sequencer is missing, please contact the administrators.


## Sample Sequencing registration

To add a new sample sequencing, either press the "+ new sample sequencing" button in the sample table, or go to NGS-specialist in the left menu, select sample sequencing, and press the button "+ add sample sequencing"

You should add the following information:

* **Vial id**: Alphanumeric identifier that marks the vial originated from the sample. The vial contains the portion of analyte extracted from the sample. Example) A, or B if it is the second vial, and so on. Numbers are also allowed.
* **Sample**: Sample from where the vial is generate (ita should have been already imported).
* **Analyte**: Molecule used for sequencing, (DNA, RNA).
* **Sequencing batch:**: The vial will be sequenced with other samples in a batch of samples. This field keeps track of what samples were sequenced together.
* **Sequencing status:** Sequencing status. Success: the sequencing was performed succesfully; Fail: the sequencing failed, or pending: sequencing not yet performed. This flag is automatically updated once the raw data is received and validated.
* **Ngs specialist**: NGS specialist who performs the sequencing and updates the current database.

Once the sample sequencing is saved, a unique barcode will be generated. Only a few fields can be modified after the barcode generation. Only the curator of the sample sequencing and the administrators can update a sample. If you need to remove a sample sequencing, please contact the administrators.


## Removing data

If you made an error and want to remove an entry (sample, patient or sequencing), please contact the administrator. Note that the entry will not be actually deleted but flagged and hidden to the users.
