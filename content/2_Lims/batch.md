+++
title = "Batch upload"
pre ="<i class='fab fa-wpforms'></i>&nbsp"
weight = 2
+++

It is also possible to load the information about patients, samples and patients sequencing from an Excel spreadsheet.


The upload file template contains on each row the data about patient, sample and sample sequencing. The patient data should be repeated for each sample/sample sequencing, e.g. :

|patient data|sample  data|sample sequencing  data|
| ------          | ------         |------ |
|patient1 data|sample 1 data|sample sequencing 1 data|
|patient1 data|sample 1 data|sample sequencing 2 data|
|patient1 data|sample 2 data|sample sequencing 3 data|

When the file is uploaded, if the barcode field is empty, the associated element is created. If the barcode is provided, then the loader search for the patient in the database (error if the patient is not found) and ignore the rest of the patient data on the row. Same thing for the sample barcode/data. this means that you can enter manually information about the patients, and load in batch the associated sample/sample sequencing information.

**example 1:**

| patient barcode|patient CRO ID|patient IRCCS|sample data|sample sequencing data |
| ------          | ------         |------ |------ |------ |
|MELA124ABC|||sample data1|sample sequencing data1 |


The loader will look for patient MELA123ABC in the database, and create associated sample 1 and sequencing 1


**example 2:**


|patient barcode|patient CRO ID|patient IRCCS|sample data|sample sequencing data|
| ------          | ------         |------ |------ |------ |
|MELA123ABC|IEO_000|IEO|sample data1|sample sequencing data1|


Same behavior as example 1: the patient barcode is provided so the associated patient data is ignored.


**example 3:**


| patient barcode | patient CRO ID | patient IRCCS | sample data | sample sequencing data |
| ------          | ------         |------ |------ |------ |
| | IEO_000 | IEO | sample data1 | sample sequencing data1 |


a new patient is created, with new sample 1 and new sequencing 1

Once the upload is successful, you can download the data from lims: the file will be the same one completed with the barcodes.



### Interface:

A link to the upload page is available from the main menu.

![acc upload interface screenshot](/img/upload_interface_screenshot.png?height=400px&classes=shadow)

The template can be downloaded either in Excel (recomended) or csv format. In the Excel template, dropdown menus are provided to select values from a controlled vocabulary (the controlled vocabularies are stored in additional sheets of the file and should not be modified).

The template is divided in three parts: 

* **patient data** : yellow
* **sample data** : red
* **sample sequencing data** : green

The csv template contains only semicolon-separated headers. Note trailing semicolon on each line - it's required for future excel compatibility.

![acc excel template screenshot](/img/excel_template_screenshot.png?height=170px&classes=shadow)


### Data Validation:

Once the user submit a file, a data validation is applied before to actually commit it to the database. The data is inserted only if ALL the entries are validated. If the validator find an error on any row, nothing will be commited, and a message will be displayed.

#### headers, CV, mandatry fields: 

* The column / header list is checked for correct spelling.
* Non-nullable fields are checked to contain actual value.
* All the values that refer to the controlled vacabulary values in the DB (institution names, patient types, sample preservation, etc.) are checked for correctness (existence in the DB).

All the invalid values in all the records are reported back to the user as one list, and no processing happens.

![acc user error invalid data screenshot](/img/user_error_invalid_data_screenshot.png?height=400px&classes=shadow)

#### Barcode validation:

A check for new / existing record will be performed. If the patient barcode is not provided, a new patient record will be inserted. If the patient barcode is provided, the sample and sample sequencing should be added to the existing patient. Similarly, a new sample will be created or an existing sample will be retrieved according to the absence of presence of the sample barcode respectively. 

Note that:
* If a barcode is submitted for a patient of a sample, the uploader will **NOT** verify that the associated data in the spreadsheet is the same as the one in the database.
* If no barcode is provided, the uploaded will **NOT** verify is the data in the spreadsheet was already submitted.

There is a limit of 10 sample sequencing per a given sample, whether they are uploaded in one or more files.

![acc user error existing record screenshot](/img/user_error_existing_record.png?height=330px&classes=shadow)


If the validation is successful, the data will be inserted in teh database, and some statistics on the transaction displayed: file a, patients records inserted: b, sample records inserted: c, sample record inserted d

![acc upload success screenshot](/img/upload_success_screenshot.png?height=370px&classes=shadow)


You can download the data from the LIMS on the statistics page (download button for each institution), including the generated barcodes. The format is the same as for the upload. 

![acc download button screenshot](/img/download_button_screenshot.png?height=400px&classes=shadow)
