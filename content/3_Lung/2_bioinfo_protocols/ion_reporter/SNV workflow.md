+++
title = "SNV workflow"
weight=3
+++

Author: @ivan.molineris

# Setup 

## Connect the S5 torrent browser to ionreporter

Create an account on [ionreporter cloud](https://ionreporter.thermofisher.com) (IR) or your local installation.

From the S5 web interface connect the account form the [Upload to IR] menu.

Upload the bam file from S5 to the desired account on IR.


![connect_ionreporter](https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/img/snv_workflow/connect_ionreporter.png)

## Upload a target regions BED file workflow preset

This step is required before the import of the workflow.

Download the [WG_IAD97207.20161004.spikeinCL.designed.bed](https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/ionreporter_workflow/lung/WG_IAD97207.20161004.spikeinCL.designed.bed) file.

Upload the file following the [instruction on the IR guide](https://ionreporter.thermofisher.com/ionreporter/help/GUID-02EF9C9F-F9A4-49D4-9AE7-3CDA57F5D59E.html):

>>>

You can upload a target regions BED file from a copied workflow and then create a target regions BED file workflow preset for use in other workflows.

Use only BED file names that do not contain spaces. If a file name includes spaces, the analysis fails.

1. In the __Workflows__ tab, click __Create__.
1. In the __Research Application__ column, click __DNA__. Click any type of sample group.
1.  For __Reference__, select __hg19__.
1.    In the __Target Regions__ section, click __Upload__.
1.    In the __Upload Target Regions File__ dialog:
  *      Select your library type or technology.
  *      Click the __Select File__ button, then browse to your BED file.
  *      Ensure that the correct BED file name appears in the display field, then click __Upload__.
        Ion Reporter™ Software uploads and verifies your BED file. When verification is complete, the progress bar changes to green, and a message confirms the files are ready for use.
  *     Click __Close__.
 The new BED file preset appears in the Target Regions section of the Reference step.
1. In the __Workflows__ tab, click __Presets__, then select __Annotation Sets__ > __Target Regions Files__, the new BED file is listed in the Workflow Presets table.
1. (Optional) When you have run a set of samples to validate that your preset works as you intended, select the preset in the Workflow Presets table, then click __Actions__ > __Lock__ to lock the workflow. You cannot undo a lock action.
>>>

## Import the workflow

> __WARNING__ there is not jet a _official_ workflow, you can use the last version at own risk.

> This workflow is developed starting from _CHPv2 Tumor-Normal v5.10_. The filterchain select snv, mnv, indel e longdel whit Allow MNPs/Allow Complex/Output MNV equal TRUE (Parameters > Variant finding (Advanced)).
The filterchain is further relaxed: Parameters > Variant finding (Main) > SNP Strand Bias equal 0.6.

Download the [workflow zip file](https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/ionreporter_workflow/lung/TargetRegions_AmpliSeq_CHPv2_tumor-normal_pair_VO_OUTMNP.zip).

Import the workflow in the __Workflow__ > __Overwiew__ tab
![import_workflow](https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/img/snv_workflow/import_workflow.png)

Other detailed instruction on [instruction on the IR guide](https://ionreporter.thermofisher.com/ionreporter/help/GUID-94319BC6-9302-4BDD-98FA-042F0BA53B69.html)

If you get an error regarding the target bed file after the .zip file upload try to logout, close the browser, open the blrowser, login and import again workflow file.

# Analysis

1. Go to __Analysis__ > __Launch analysis__ > __Manual__
![launch_analysis](https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/img/snv_workflow/launch_analysis.png)
2. Select the workflow you imported
3. Go to __Samples & Relationship __
4. Select a pair of tumor and normal samples of the same patient
5. Add the selectd samples to the analysis with __Add samples__
![sample_relationships](https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/img/snv_workflow/define_sample_relationship.png)
6. Add a name of the patient to the analysis
7. Go to 4 and repeat until there is one analysis loaded for each patient
8. Submit the analyses




