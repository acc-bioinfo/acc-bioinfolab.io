+++
title = "CNA workflow on IR for amplifications"
weight = 2
+++

Author: Giorgia Mori

### Rationale ###
Preliminary analyses by Giorgia Mori suggested that the Ion Reporter pipeline described below performs well to call amplifications. No IR pipelines were found to call deletions on the samples used for benchmarking (mainly Horizon samples). For deletions it was found that CNVkit may perform better (see this [presentation](https://gitlab.com/acc-bioinfo/meta/tree/master/docs/Presentations/2018_September_acc-bioinfo_update.pdf)). Therefore, it was proposed to use the Ion reporter CNA pipeline described below to call amplifications and [CNVkit to call deletions](https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/tree/master/content/ACC_lung_project/bioinfo_protocol/Workflows_SNV_CNA_Fusions/CNA_workflow_CNVkit_for_deletions).


### The Target Region File (.bed file) ###

For CNA call, the Target Region File (bed file) should contain this mandatory field:

- GENE_ID=name

which should be the 5th column. Here you have to specify the name of the gene present in the amplicon.  

- PURPUSE=CNV

is optional and should be the 6th column. It works like a 'hotspot' for CNA call. 


Your .bed file should look like this:

| | | | | | |
|---|---|---|---|---|---|
| chr1 | 40362511 | 40362630 | MYCL1_13.3.70 | GENE_ID=MYCL;Pool=1 | PURPOSE=CNV |

The **ACC lung Target Regions File** used in the workflow *wfCNA-06-IR5.10* and modified for CNA call is named ```WG_IAD97207.20161004.spikeinCL.designedTRIS``` and can be downloaded from the link present in the table below.  

### The Baseline ###

The baseline ```CNVBase_18_male_samples``` used in *wfCNA-08-IR5.10* was trained using 18 male samples. You can download this baseline from the link present in the table below. 

If you want to create your own baseline see - *Appendix -> Create a CNA baseline*.


### The Filter Chain ###

For **somatic CNA call**, a custom Filter Chain should be set before starting with workflow creation. 
On IR, click on:

Workflows -> Presets -> Create Preset -> Filter Chain

- Give a name to the Filter Chain (e.g MyFilterChain)
- Select hg19 Reference
- Select *CNV Confidence Range - CNVs Only*
- Do not modify default parameters and click on 'Set'
- Click Save

![Screen_Shot_2018-09-03_at_15.58.26] (https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/img/Screen_Shot_2018-09-03_at_15.58.26.png)

Now you can start creating your workflow.

**N.B.** in order to reduce False Positives (FPs) call you can modifiy the range of the Filter Chain choosing as the lower value **30** instead of 10. 
If you have a sample with a **subclonal mutation**, choose as lower value **20** instead of 10.

## Workflow description ##

This is the description of the ```wfCNA-08-IR5.10``` workflow used to call CNA. **Click on the links to download the files exported from IR 5.10**. 

| | |
| --- | --- | 
| WorkflowID | [wfCNA-08-IR5.10] (https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/docs/acclung/wfCNA-08-IR5.10.zip) |
| Workflow description | Single sample CNA workflow |
| Research application | DNA |
| Sample groups | Single |
| Reference genome | hg19 |
| Reference panel design | **ACC Lung target region file** [WG_IAD97207.20161004.spikeinCL.designedTRIS] (https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/docs/acclung/target_region_WG_IAD97207.20161004.spikeinCL.designedTRIS.bed) |
| Annotation | all |
| MyVariant database | MyVariantDefault_hg19 |
| Filter chains | MyFilterChain - CNV Confidence Range - CNVs Only (10.0 <= CNV Confidence Range - CNVs Only <= 1.0E7)   |
| Baseline | [CNVBase\_18\_male\_samples] (https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/docs/acclung/CNVBase_18_male_samples.zip)
| Final Report | default |
| Parameters | default |
| IR version  | 5.10 |
| IR account username | Todaro.Vincenzo@20mail.it |
| Created by| Giorgia Mori |
| Creation date | 06/07/2018 |
| Other comments | Validation results: All amplifications detected. **No deletions detected**. | 


## Step-by-step instructions to create the workflow on Ion Reporter ##

**Research application page** 

- Select Research application: *DNA*
- Select Sample Groups: *Single*

![Screen_Shot_2018-09-03_at_14.27.51] (https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/img/Screen_Shot_2018-09-03_at_14.27.51.png)

**Reference**


- Select Reference: *hg19*
- Upload panel design file, **the same you used to create your baseline**.

![Screen_Shot_2018-09-03_at_14.28.51] (https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/img/Screen_Shot_2018-09-03_at_14.28.51.png)

**Annotation page** 

Default options:

- Annotation set: *all*
- MyVariants Database: *MyVariantDefaultDb_hg19*

![Annotation_4](https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/img/Annotation_4.png)

**Filters**

- Select Filter chain: *MyFilterChain*

![Screen_Shot_2018-09-03_at_14.30.39] (https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/img/Screen_Shot_2018-09-03_at_14.30.39.png)

**Baseline**

- Select your baseline

![Screen_Shot_2018-09-03_at_14.30.51] (https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/img/Screen_Shot_2018-09-03_at_14.30.51.png)

**Plugin page** 

- No plugins: just click on next

![Screen_Shot_2018-09-03_at_14.31.03] (https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/img/Screen_Shot_2018-09-03_at_14.31.03.png)

**Final report**

- Select Final Report Template

![Screen_Shot_2018-09-03_at_14.31.59] (https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/img/Screen_Shot_2018-09-03_at_14.31.59.png)

Save the worflow (for the format of the worflow name see [WorkflowID](https://acc-bioinfo.gitlab.io/documentation/workflow_id/)); e.g. ```wfCNA-08-IR5.10.zip``` .



