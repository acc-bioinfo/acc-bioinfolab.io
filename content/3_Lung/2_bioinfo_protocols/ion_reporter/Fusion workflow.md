+++
title = "Fusion workflow"
weight=1
+++

Author: Laura Fancello

## Workflow description ##

This is the description of the workflow used to call gene fusions.



| | |
| --- | --- |
| WorkflowID | wfFUS-00-IR5.10 |
| Workflow description | Single samples fusions workflow |
| Research application | Fusions |
| Sample groups | Single |
| Reference genome | hg19 |
| Reference panel design | ACC\_lung\_translocations\_IEO\_040116\_Designed.bed |
| Annotation | all |
| MyVariant database | MyVariantDefault_hg19 |
| Filter chains | Default Fusions View |
| Final Report | default |
| Parameters | default |
| IR version  | 5.10 |
| Workflow name on IR | Fusions_Single\_hg19\_WG\_WG00209\_1\_All\_FilterDefaultFusionsView\_OnIR5.10 |
| Created by| Laura Fancello |
| Creation date | 21/06/2018 |
| Other comments | validation results: all fusions detected | 


## Step-by-step instructions to create the workflow on Ion Reporter ##


**Research application page** 

Select Research application: *Fusions*

Select Sample Groups: *Single fusions*


![createworkflow2](https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/img/CreateWorkflow_2.png)


**Reference**


Select Reference: *hg19*



Fusion panel: upload panel design file [ACC_lung_translocations_IEO_040116.zip](https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/docs/acclung/ACC_lung_translocations_IEO_040116.zip) 


![UploadReference_3](https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/img/UploadReference_3.png)

**Annotation page** 



Default options:



Annotation set: *all*



MyVariants Database: *MyVariantDefaultDb_hg19*



![Annotation_4](https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/img/Annotation_4.png)

**Filters**



Select Filter chain: *Default Fusions View*


**Plugin page** 



No plugins: just click on next

![Plugins_6](https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/img/Plugins_6.png)

**Final report**



Select Final Report Template

![FinalReport_7](https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/img/FinalReport_7.png)

Save the worflow (for the format of the worflow name see [WorkflowID](https://acc-bioinfo.gitlab.io/documentation/workflow_id/)); e.g. ```wfFUS-00-IR5.10.zip``` 


## Files ##

Link to file .zip exported from Ion reporter and containing the workflow: [```wfFUS-00-IR5.10.zip```](https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/docs/acclung/wfFUS-00-IR5.10.zip)
