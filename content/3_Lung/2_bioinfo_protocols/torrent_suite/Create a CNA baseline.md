+++
title = "Create a CNA baseline"
weight=3
+++

Author: Giorgia Mori

For baseline creation you have to select **five or more normal male samples** (10-12 samples are recommended).

From Ion Reporter:

- In the **Workflows** tab, in the **Presets** screen, click **Create Preset**, then select **Copy Number Baseline** from the dropdown list;

- Click **AmpliSeq**, then select your Target Regions file, then click Next;

![Screen_Shot_2018-09-04_at_10.53.57] (https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/img/Screen_Shot_2018-09-04_at_10.53.57.png)

- Select at least 5 normal male samples, then click Next;

- Enter a name for your baseline, then click **Create Baseline**.

![Screen_Shot_2018-09-04_at_10.59.24] (https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/img/Screen_Shot_2018-09-04_at_10.59.24.png)

- In Workflows -> Presets -> Copy Number Baselines check the status of your baseline, when it is successful, you can start to create your workflow. 

