+++
title = "Create a CSV samplesheet for automatic Ion Torrent upload"
weight=2
+++

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
Author: Giorgia Mori

Create a CSV samplesheet with **sample name**, **cellularity**, **gender** and **ACC-barcode** fields to be uploaded to IR. 

## Step 1: Create the CSV samplesheet

* create a table with LIMS informations ([https://bioinfo.ieo.it/accbarcode/samplesequencing/] (https://bioinfo.ieo.it/accbarcode/samplesequencing/))

* add cellularity ([https://bioinfo.ieo.it/accbarcode/sample/] (https://bioinfo.ieo.it/accbarcode/sample/))

* add Ion barcode (IonXpress xxx) of each sample ([see template format] (https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/docs/acclung/plan_samples_2018_08_30_14_59_30.csv))

* save as .csv

You should obtain something like this:


`required fields`

|`Barcode`|Control Type|`Sample Name`|Sample ID|Sample Description|DNA/RNA/Fusions|Reference library|Target regions BED file|Hotspot regions BED file|Cancer Type|Cellularity %|Biopsy Days|Couple ID|Embryo ID|IR Workflow|IR Relation|IR Gender|IR Set ID|
|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|
|IonXpress_001||lung14SB3O-1TBR214|lung14SB3O|somatic biopsy FFPE RNA|DNA|hg19|WG_IAD97207.20161004.spikeinCL.designed|||70||||||Female||
|IonXpress_002||lung14SB3O-1TAD114|lung14SB3O|somatic biopsy FFPE DNA|DNA|hg19|WG_IAD97207.20161004.spikeinCL.designed|||70||||||Female||
|IonXpress_003||lung14SB3O-2NAD114|lung14SB3O|germline blood DNA|DNA|hg19|WG_IAD97207.20161004.spikeinCL.designed|||100||||||Female||
|IonXpress_004||lung14Y2OX-1TBR214|lung14Y2OX|somatic biopsy FFPE RNA|DNA|hg19|WG_IAD97207.20161004.spikeinCL.designed|||70||||||Male||
|IonXpress_005||lung14Y2OX-1TAD114|lung14Y2OX|somatic biopsy FFPE DNA|DNA|hg19|WG_IAD97207.20161004.spikeinCL.designed|||70|||||Male|
|IonXpress_006||lung14Y2OX-2NAD114|lung14Y2OX|germline blood DNA|DNA|hg19|WG_IAD97207.20161004.spikeinCL.designed|||100||||||Male||
|IonXpress_007||lung14TJOL-1TBR214|lung14TJOL|somatic biopsy FFPE RNA|DNA|hg19|WG_IAD97207.20161004.spikeinCL.designed|||30||||||Male||
|IonXpress_008||lung14TJOL-1TAD114|lung14TJOL|somatic biopsy FFPE DNA|DNA|hg19|WG_IAD97207.20161004.spikeinCL.designed|||30|||||Male||
|IonXpress_009||lung14TJOL-2NAD114|lung14TJOL|germline blood DNA|DNA|hg19|WG_IAD97207.20161004.spikeinCL.designed|||100||||||Male||
|IonXpress_010||lung146WIS-1TBR214|lung146WIS|somatic biopsy FFPE RNA|DNA|hg19|WG_IAD97207.20161004.spikeinCL.designed|||70|||||Female||
|IonXpress_011||lung146WIS-1TAD114|lung146WIS|somatic biopsy FFPE DNA|DNA|hg19|WG_IAD97207.20161004.spikeinCL.designed|||70|||||Female||
|IonXpress_012||lung146WIS-2NAD114|lung146WIS|germline blood DNA|DNA|hg19|WG_IAD97207.20161004.spikeinCL.designed|||100||||||Female||


## Step 2: on Ion Torrent interface

* Plan Runs

* Template Runs

* ACC lung

* select Reference Library and target region

* add barcode number (usually 12) and chip ID:

![Screen_Shot_2018-08-30_at_14.37.36] (https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/img/Screen_Shot_2018-08-30_at_14.37.36.png)

* click on "Load Sample Table" and select the CSV file you have previously created:

![Screen_Shot_2018-08-30_at_14.38.32] (https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/img/Screen_Shot_2018-08-30_at_14.38.32.png)

* all of the fields should be automatically compiled:

![Screen_Shot_2018-08-30_at_14.41.31] (https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/img/Screen_Shot_2018-08-30_at_14.41.31.png)





 

