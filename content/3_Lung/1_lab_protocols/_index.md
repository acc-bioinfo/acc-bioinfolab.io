+++
title = "Laboratory protocol"
pre ="<i class='fa fa-flask'></i>&nbsp"
weight=1
+++

Download from the following link the library preparation protocol for the **ACC-lung project**:

### Latest

* [Lab protocol - Version 2.0 ](https://gitlab.com/acc-bioinfo/lab-protocols/acc-lung/raw/master/ACC-Lung_labprotocol_V2.doc)
* [Quick reference - Version 2.0](https://gitlab.com/acc-bioinfo/lab-protocols/acc-lung/raw/master/ACC-Lung_QuickRef_V2.doc)
*  [ACC PROCEDURA Manuale procedure di laboratorio - Version 4.0](https://gitlab.com/acc-bioinfo/lab-protocols/acc-lung/raw/master/ACC_Manuale_procedure_di_laboratorio_v._4.0_final.pdf)


#### Obsolete versions

Other versions are available in the public repository on gitlab: [Lab protocols](https://gitlab.com/acc-bioinfo/lab-protocols/acc-lung/).



### Contact info

Project Coordinator for the laboratory component of the project: [Gianmaria Frigè - IEO](mailto:gianmaria.frige@ieo.it)
