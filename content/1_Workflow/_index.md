+++
title = "ACC Genomics Workflow"
pre ="<i class='fa fa-project-diagram'></i>&nbsp"
weight = 1
+++

{{% alert theme="info" %}} The following work was inspired by the [TCGA project barcode system](https://wiki.nci.nih.gov/display/TCGA/TCGA+barcode) {{% /alert %}}

We propose the following **ACC-barcode** representation, designed to organize together the NGS data produced by different sequencing hospitals in the Italian ACC network.

The ID is composed of different blocks, which represent each stage of the sample processing workflow. The **ACC-barcode** is progressively composed by adding blocks of code at each step of the workflow. The constitutive parts of this barcode provided semantic values.

{{% panel header="ACC-barcode structure" %}}\<PROJECT\>\<TSS\>\<Partecipant\>-\<Sample\>\<Vial\>\<Analyte\>\<Sequencing_ID\>\<SC_ID\>{{% /panel %}}

![acc-barcode example](https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/raw/master/static/acc-barcode.png?height=150px)


{{% notice tip %}} ```brca02BEEC-3TBR102``` Can be decomposed as ```<brca><02><BEEC><3T><B><R><1><02>```, which identifies the patient with identifier **BEEC** is recruited in the ACC **brca** project, the sample was collected from the "Instituto Nazionale dei Tumori". The sample has assigned the unique identifier **3** and is **somatic** (T). This indicates that the patient had already other 2 samples collected. The **analyte** was extracted in the vial **\<B\>**. The **<R>** in this case indicates that the analyte is **RNA**. The following 1 indicates that this sample, in this vial, with this analyte was sequenced for the first time with sequencing ID **\<A\>**. Finally, the sample was sequenced at the "Instituto Nazionale dei Tumori" (SC code: **02**).
{{% /notice %}}

## Data processing workflow

The proposed data workflow can be resumed in the following image:

{{<mermaid align="left">}}
sequenceDiagram
    participant Oncologist
    participant CRO
    participant NGS specialist
    note left of Oncologist: Stage1: patient recruitment
    Oncologist->>CRO: New patient
    Note right of CRO: new CRO_ID
    CRO->>Oncologist: Provides CRO_ID
    Oncologist->>NGS specialist: Initiate request
    Note right of NGS specialist: Stage2: Sample processing
    Note right of NGS specialist: Stage3: Sequencing
    NGS specialist ->> CRO: Provides ACC-barcode
    NGS specialist ->> Oncologist: Provides Results
{{< /mermaid>}}

More specifically, it can be divided in 3 stages that are required to generate the ACC-barcode:



### Stage 1 - Patient recruitment

* Patient is recruited by an oncologist in the ACC network;
* The **Oncologist** contacts the **CRO**;
* **CRO** creates a new entry in the CRO database and generate the *CRO_id* which is provided to the **oncologist**;
* **Oncologist** communicates the *CRO_id* to **NGS specialist**;
* **NGS specialist** connects to the ACC *web-platform* and fills the first block of the form adding the *CRO_id*.


### Stage 2 - Sample information

The **NGS specialist** adds to the form the sample information. Among the information that will be recorded, we list hereby some of the most relevant:

  * Sample type (tumor/normal);
  * Conservation (FFPE/FROZEN);
  * Primary Tissue;
  * Cellularity;
  * Data sample was collected.

This simulates the laboratory accepting a sample for the studying and storing it waiting for it to be processed (next phase).


### Stage 3 - Sample processing

The **NGS specialist** starts processing the sample information. New information will be added to the form. We report hereby some of the most relevant:

* Create a vial identifier;
* Analyte (DNA, RNA);
* Specify genomic panel to be used for sequencing;
* Add Sequencing Site.

The *vial Idenfitier* for the probe containing the extracted analyte. From each sample, the NGS specialist could extract multiple portion of the analyte.


The Bam files generated will use the ACC-barcode.

* Finally the CRO

LA CRO viene notificata che il CRO_ID ha completato il processo di analisi. La CRO può quindi sbloccare nuovi reagenti ai centri (permettendo il rilascio di nuovi CRO_ID). Giustificare il risultato dell’analisi (sia che i campioni abbiano fallito che no).
