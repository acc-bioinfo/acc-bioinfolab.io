+++
title = "Laboratory protocol"
pre ="<i class='fa fa-flask'></i>&nbsp"
weight=1
+++

Download from the following link the library preparation protocol for the **ACC-Gersom project**:

### Latest


- [ACC Gersom library prep](https://gitlab.com/acc-bioinfo/lab-protocols/acc-gersom/-/blob/main/ACC_GerSom_LibPrep_v1_12012021.pdf)
- [ACC GerSom Manuale Fissazione a Freddo](https://gitlab.com/acc-bioinfo/lab-protocols/acc-gersom/-/blob/main/ACC_GerSom_Manuale_Fissazione_a_Freddo_-_WG_Pathology_v._2_13092021_clean.pdf)
- [ACC GerSom Manuale Processamento Sangue](https://gitlab.com/acc-bioinfo/lab-protocols/acc-gersom/-/blob/main/ACC_GerSom_Manuale_Processamento_Sangue.pdf)

#### Obsolete versions


### Contact info

Project Coordinator for the laboratory component of the project: [Gianmaria Frigè - IEO](mailto:gianmaria.frige@ieo.it)
