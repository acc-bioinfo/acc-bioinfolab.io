+++
title = "Torrent Suite"
pre ="<i class='fa fa-lungs'></i> "
weight=1
+++


Next-generation sequencing (NGS) utilizes massively parallel sequencing to generate thousands of megabases of sequence information per day. Torrent Suite Software provide automated sequencing data analysis simply and fast for Ion sequencer series: Ion S5, Ion S5 XL, Ion PGM and Ion Proton systems. 

Torrent Suite Software is directly connected to the sequencer and allows:

* to plan sequencing run information including run details, sample identification name and automated data analysis preferences;
* monitor key metrics to help evaluate the status of the run;
* view sequencing run results as easy-to-read graphics and key performance metrics.

At the end of sequencing Torrent Suite Software processes signals, assigns nucleobases to chromatogram peaks (BaseCaller), aligns sequences with genome reference and calls when a nucleotide difference versus a reference at a given position in an individual genome or transcriptome (Variant Calling). The usual output of these procedures are BAM (Binary Sequence Alignment/Map) and VCF (Variant Call Format) files, easily downloading for each samples from Torrent Suite together the run report. Torrent Suite Software expanded its capabilities with verified plug-ins available to manage a full range of additional applications and analyses (example for RNA-Seq, Chip-Seq, Epigenetics). It has seamless integration with Ion Reporter Software.
