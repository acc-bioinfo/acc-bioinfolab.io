+++
title = "CNA workflow by CNVkit for deletions"
weight=2
+++

Author: Laura Fancello


### Rationale ###
Preliminary analyses by Giorgia Mori suggested that CNVkit may perform better than Ion Reporter pipelines to call deletions, but not for amplications (see this [presentation](https://gitlab.com/acc-bioinfo/meta/tree/master/docs/Presentations/2018_September_acc-bioinfo_update.pdf)). Therefore, it was proposed to use [an Ion Reporter pipeline to call amplifications](https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/tree/master/content/ACC_lung_project/bioinfo_protocol/Workflows_SNV_CNA_Fusions/CNA_workflow_on_IR_for_amplifications) and CNVkit to call deletions.


### Installation ###
It is **already installed on Cineca** in the ACC conda environment ```acctoolbox```
You find [here](https://gitlab.com/acc-bioinfo/acc-welcome/wikis/hpc-infrastructure) instructions on how to activate the ACC conda environment.


Otherwise, CNVkit is available as a docker image ([https://hub.docker.com/r/etal/cnvkit/](https://hub.docker.com/r/etal/cnvkit/)) or source code from github ([https://github.com/etal/cnvkit](https://github.com/etal/cnvkit)).



### Input ###
It needs in input:

1. fasta file of reference genome (file for hg19 already present in ```/marconi_work/ELIX3_ACC-Bio/CNVkit/```)
2. bed file with panel design (file for ACC Lung panel already present in ```/marconi_work/ELIX3_ACC-Bio/CNVkit/```)
3. bam files tumors and normals (currently in ```/marconi_work/ELIX3_ACC-Bio/ACCDropBox```)
4. annotation file (i.e. refFlat.txt) for gene annotation, required only if panel design does not include gene names. Not required for ACC Lung panel (gene names already present in the bed file with panel design). If necessary, refFlat.txt for hg19 already present in ```/marconi_work/ELIX3_ACC-Bio/CNVkit/```
5. information on cellularity


### Usage ###

CNVkit **must** be used with the option ```--method amplicon``` for amplicon-based panel sequencing such as ACC Lung.

The option ```-p N```(i.e. -p 4) allows to spread the workload across all available CPUs.


**Commandline:**

**1. Sort bam files**

**2. Calculate log2 ratio copy numbers** (log2 of the ratio observed copy number/expected copy number, which is 2 in diploidy)
The following command builds as reference (baseline) a pooled reference from all the normal samples specified with --normal option.
The authors recommended combining all normal samples into a pooled reference, even if matched tumor-normal pairs are available.
Segmented log2 ratio copy numbers are given in output (.cns and .cnr files).

```cnvkit.py batch <tumor bam files> --normal <normal bam files> --method amplicon -t <panel_Design.bed> --fasta <reference_genome.fasta> --output-dir <results_directory> -p N```

**3. Convert log2 ratio copy numbers to integer copy numbers** refining the estimate by **tumor purity** information (cellularity).
Note that this is an optional step and it was not performed in Giorgia Mori benchmarking presented on [September 2018](https://gitlab.com/acc-bioinfo/meta/tree/master/docs/Presentations/2018_September_acc-bioinfo_update.pdf)
```cnvkit.py call <TumorSample>.cns -m clonal --purity <cellularity> -o <TumorSample>.call.cns```


If lacking cellularity information you can convert segmented log2 ratio copy numbers into integers, using **default thresholds**:
```cnvkit.py call <TumorSample>.cns -m threshold -t=-1.1,-0.4,0.3,0.7 -o <TumorSample>.call.cns```

These thresholds can only be reasonable for samples with cellularity >= 40%

| log2 value threshold | copy number |
| ---| --- |
| -1.1 | 0 (homozygous deletion) |
| -0.4 | 1 (heterozygous deletion) |
| 0.3 | 2 (diploid) |
| 0.7 | 3 (amplification: one copy gain) |
| > 0.7 | >4 (amplification: two or more copy gain) |



**4. Export copy number calls into VCF or BED formats** for downstream analyses. For example,
in order to perform benchmarking by the [cnaBenchamrking tool](https://gitlab.com/acc-bioinfo/Clinical_utility_toolkit/cnaBenchmarking) 
you need to convert .txt output of CNVkit into a vcf file.

```cnvkit.py export vcf <TumorSample>.call.cns -o <TumorSample>.call.vcf```
```cnvkit.py export bed <TumorSample>.call.cns -o <TumorSample>.call.bed```


For further information and options please refer to [https://cnvkit.readthedocs.io/en/stable/index.html](https://cnvkit.readthedocs.io/en/stable/index.html).





**Issues/Open questions/to dos** 

1. introduce quality control of normal samples used to create reference (baseline), using ```cnvkit.py metrics *.cnr -s *.cns``` to see if any samples are especially noisy. 
2. Which normal samples to create reference (baseline)? Always the same to guarantee reproducibility or all available normal samples at the time of each analysis to increase representativeness and confidence?
Should we create centralized reference of normal samples to be used by all IRCCS?
3. **Not all the options were tested and further refinement of the pipeline and validation is required**: correction of copy number estimates by tumor purity, quality control of normal smaples used to create the reference not used in the benchmarking
4. In principle one of the strength of CNVkit is the use of off targets to call copy numbers. However, off targets are not present for amplicon-based sequencing. Therefore, it's surprising that it performs better than Ion Reporter pipelines on ACC Lung panel
5. Very few samples avaiulale at the time of benchmarking.

 






  
