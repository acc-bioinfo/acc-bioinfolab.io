+++
title = "WorkflowID Specifications"
weight=2
+++


<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

  This ID keeps track of the different workflows used to analyze data. The same .bam file can be analyzed with more than one workflow.


  {{% panel header="WorkflowID specification" %}}\<wf\>\<TYPE\>\-\<VN\>\-\<IR\>\{{% /panel %}}



| Field | Description | Values |
| --- | --- | --- |
| wf | fixed string, stands for workflow | wf |
| TYPE | type of variant called by the workflow | CNA, SNV, FUS |
| VN | workflow version (progressive 2 digits number) | 00, 01, 02, 03, ... |
| IR | Ion Reporter version | IR5.2, IR5.6, IR5.10, ... |





  {{% notice tip %}}```wfCNA-01-IR5.6``` Can be decomposed as ```<wf><CNA><01><IR5.6>```, which  would mark the workflow for CNA calls, second version (the first is 00) created on Ion Reporter version 5.6.
  {{% /notice %}}
