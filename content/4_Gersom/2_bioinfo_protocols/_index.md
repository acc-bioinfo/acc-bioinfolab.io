+++
title = "Bioinformatics protocol"
pre ="<i class='fas fa-laptop-medical'></i>&nbsp"
weight=3
hidden = true
+++


Protocols for Torrent Suite and Ion Reporter.


Download the full [Gersom SOP v1.0](https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io/-/raw/master/static/docs/gersom/SOPs_Gersom_1.0.pdf?inline=false).

