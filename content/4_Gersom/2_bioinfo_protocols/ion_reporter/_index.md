+++
title = "Ion reporter "
weight=2
+++

The integration of Ion Reporter Software with Torrent Suite Software is built to complete the next-generation sequencing workflow from sequencing data to report. Ion Reporter Software performs controlled analysis, annotation with genome reference, and reporting of variants along with application-specific workflows to support single, trio, and paired tumor–normal analysis. Push-button data analysis with easy-to-use preconfigured and customizable workflows, providing flexibility for users of any experience level. Easily identify relevant variants with annotations from >20 public and proprietary databases (COSMIC, OMIM , EXAC ecc.) . Fast results with optimized workflows and user tools . Quickly focus on your variants of interest with simple result filtering. Then save your filter settings for standardized workflows. It allows to visualize variants in Integrative Genomics Viewer (IGV), or easily compare variants across samples with a variety of interactive variant heat map or Venn diagram options. Select relevant variants and create interpretive research reports, or export variants to spreadsheet, VCF, or text format for further investigation.

