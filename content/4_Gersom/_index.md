+++
title = "ACC-Gersom"
pre ="<i class='fa fa-heartbeat'></i>&nbsp"
weight = 4
+++

Laboratory and bioinformatics protocols for the ACC Gersom projects.
