+++
title = "Laboratory protocol"
pre ="<i class='fa fa-flask'></i>&nbsp"
weight=1
+++

Download from the following link the plasma preparation protocol for the **ACC-ACCORD project**: 

* [PROCESSAMENTO CAMPIONI PLASMA - PROTOCOLLO DI STUDIO ACC ACCORD](https://gbox.garr.it/garrbox/s/EjVHumwZidripo5)
