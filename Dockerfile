FROM alpine

ENV HUGO_VERSION '0.40'

RUN apk update && apk add openssl ca-certificates
RUN wget -O ${HUGO_VERSION}.tar.gz https://github.com/spf13/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_Linux-64bit.tar.gz
#- echo "${HUGO_SHA}  ${HUGO_VERSION}.tar.gz" | sha256sum -c
RUN tar xf ${HUGO_VERSION}.tar.gz && mv hugo* /usr/bin/hugo
# * Install my theme; need to first install git and then do git submodule update for that.
# RUN apk add git && \
#  git submodule init && \
#  git submodule update --force

WORKDIR /usr/src/app