CONTRIBUTE GUIDELINES and HOW TO
================================================================================

Launch a local instance of the website for testing and previewing Changes
--------------------------------------------------------------------------------

1. Clone the repository to your local machine
```
git clone https://gitlab.com/acc-bioinfo/acc-bioinfo.gitlab.io
```
2. Install HUGO, you can fetch it from here or from the latest release, pick your distribution (e.g. linux-64-deb)
https://github.com/gohugoio/hugo/releases
3. Run the following command from the project root folder.

```
hugo server -D
```

4. Webserver should be launched on: http://localhost:1313/


Make changes to the website
--------------------------------------------------------------------------------

The master branch is protected because is the production branch. So, if you want to make any change please follow this workflow:


* Clone the repository
* From gitlab, create a new issue and comment in the issue what your goal is in the modification you are about to make
* After creating the issue, click on the green button to create a new merge request, (it should be created in WIP "work in progress status", remove that we you are ready to merge your request with the master branch).
* After creating a new merge request, you have see a box on the top right ("check out branch"). After clicking on it you will see some line of code. Copy and paste in the terminal the code from **Step 1 ONLY!**. you should have something like this:

```
git fetch origin
git checkout -b <your issue title>
```

now you have created a new local branch.

* make your Changes
* preview your changes on the local instance.
* when happy, commit your changes
* Push to remote branch ```git push origin <your branch>```

* From github: click on "Resolve WIP status"
* assign **Alessandro Guida** to the merge request.  
* After reviewing Alessandro will merge it to the master branch. At that point
the changes will to into production.


Simple guide to git: http://rogerdudler.github.io/git-guide/

HOW TO ADD IMAGES
--------------------------------------------------------------------------------

All images should go into the ```static``` folder.
